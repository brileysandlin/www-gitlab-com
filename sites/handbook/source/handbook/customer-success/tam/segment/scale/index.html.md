---
layout: handbook-page-toc
title: "TAM Segment: Scale"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

⚠️ This information is under active development and is not yet final. This page will be updated on an ongoing basis during this phase.
{: .alert .alert-warning}

## Overview

Definition: Pooled TAMs driving programmatic enablement and some customer engagement.

## Motions

### Align

This cohort is primarily enabled through content and webinars, and the success plan will be driven in year 1 of a customer's lifecycle by the desired value drivers from the command plan, and will be used for strategic engagements in customers outside of onboarding.

### Enable

Onboarding and enablement for this cohort is primarily through webinar cohorts, with a TAM reach-out at day 30 to ensure the customer is on-track for success.  There are additional reach-outs for triggered events such as missed time-to-first-value and poor onboarding NPS scores.

#### Metrics for Enable

Onboarding:

1. Net-new customer attendance in onboarding webinars
1. Time to First Value
1. % 30 day calls completed
1. Onboarding NPS & CSAT scores

Use-Case Enablement:

1. Customer attendance in enablement webinars
1. Use Case Health scores

### Expand & Renew

Expansion is primarily driven by the SAL or AE in this segment, though a key driver for expansion is product enablement and familiarity.

#### Metrics for Expand & Renew

1. Customer attendance in expansion webinars
1. New Use Cases Adopted
1. Renewal NPS & CSAT Scores
