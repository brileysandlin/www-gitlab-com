---
layout: handbook-page-toc
title: "SALSATAM Meetings"
description: "Information about Account Team Meetings (SAL, SA, TAM)"
---


## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview 

## Roles and Responsibilities 

*Strategic Account Leader (SAL)*
- Leads the sales call.
- Negotiates contracts and price on initial sale and renewals.
- Should have a strategy for prospects and how to close.

*Solutions Architects (SA)*
- Should lead discovery calls to find out where and why the prospect is having issues in the SDLC
- Runs POC’s, scopes out SOW’s 
- The technical advisor to the SAL.
- Should handle all pre-sale technical activities to be handed off to the TAM after the technical win stage.
- Owns the technical side for responding to RFP/RFIs

*Technical Account Managers (TAM)*
- Trusted technical advisor to the customer.
- Should handle all things post-sales for the customer (onboarding, Stage Adoption, EBR’s, Account Reviews etc.)
- Leads regular cadence calls and keeps the customer collaboration projects up-to-date
- Maintains Gainsight records of customer health and triaging as necessary.
- Escalation PoC

### Who is in Charge of the Meeting? 

- *No specific person/role is the boss.  You are a TEAM.*
- Tasks should be agreed upon during the SALSA-TAM meeting and fall into each person’s area of responsibility.
- SA’s and SAL’s need to work together to ensure a smooth sales process… from discovery to demo to POC to final sale.
- SA’s and TAM’s need to work together to ensure a smooth transition for the customer.
- TAM’s and SAL’s need to work together to ensure a smooth onboarding, continued customer happiness, growing accounts, driving stage adoption, and to lock in the renewal for years to come.

## Working Together throughout the Customer's Lifecycle  

The SA owns all pre-sales technical relationships and activities. The SA quarterbacks technical conversations prior to the sale. TAM involvement prior to the sale models the expectations for customer relationships after the sale. TAM involvement should supplement, not displace, SA pre-sales ownership of the account. TAM engagement prior to the sale should occur in the following situations:
- During POC kickoff or when appropriate in deep conversations on technology if no POC will occur
- When a shared Slack channel is created for the customer
- A shared customer issue tracking project has been created that will affect the account long-term
- As requested by the SA if the TAM has a specific subject matter expertise relevant to the conversation

The TAM owns the account to drive adoption. SA's are to remain involved in the following situations:
- Development of expansion plans with the SAL and TAM
- A new POC or product evaluation begins for a single team or an enterprise-wide upgrade - anything affecting IACV may require SA involvement
- Any product license expansions that require overviews, education and competitive insights prior to close
- Transformative or dedicated services are being discussed to pass to SA once identified
- If a TAM is over-committed or unable to support a customer request, the SA may be requested to assist
- Support of GitLab days or other on-site evangelism of GitLab at customer sites

## SALSATAM Meeting Best Practices 

- Should be weekly
    - Sometimes there’s nothing to talk about so its ok to cancel once in a while.
    - DO NOT GET IN THE HABIT OF CANCELLING
- Should have a running doc of the meeting
    - Have a list of customers and prospects that you talk about.
    - Go down the list of customers & prospects for updates
    - TAM’s - pay attention to prospects and where they are in the sales cycle. They will be your customers soon.
    - TAM’s - focus on what renewals are coming up soon
    - SA’s - your knowledge, expertise and prior relationship with the customer can be valuable to the TAM
- Make sure to discuss health scores and any changes that may be needed
- Try to find time to contribute. Demo a new feature, talk about selling strategy, teach each other something new.
- Establish a process, and let someone different run the meeting each week
    - Example:  Prospects first, then TAM updates. Maybe sort by value, or renewal date
- Prioritize, but ensure you cover all accounts on some cadence
- Regularly check Gainsight for renewals, Triage board, SOW objects, EBR objects

## Tools to Use During the Meeting 

- [Working Agreement](/handbook/customer-success/solutions-architects/processes/#working-agreements) 
- Salesforce
- Gainsight 
- GitLab Triage Boards 
- GitLab Customer Account Management 
- GoogleDocs 

